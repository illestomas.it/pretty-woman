import * as React from "react";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { openModal, closeModal, setModalBody } from "src/pages/App/actions";

interface Props {
  dispatch: ThunkDispatch<{}, {}, AnyAction>;
  modalVisible: boolean;
  modalBody: React.ReactNode;
}

const withModal = <P extends object>(
  WrappedComponent: React.ComponentType<P>
) => {
  return class NewComponent extends React.Component<P & Props> {
    public handleCloseModal = () => {
      const { dispatch } = this.props;
      dispatch(closeModal());
    };

    public handleOpenModal = () => {
      const { dispatch } = this.props;
      dispatch(openModal());
    };

    public setContent = (node: React.ReactNode) => {
      const { dispatch } = this.props;
      dispatch(setModalBody(node));
    };

    public render() {
      const newProps = {
        handleCloseModal: this.handleCloseModal,
        handleOpenModal: this.handleOpenModal,
        renderModalContent: (node: React.ReactNode) => this.setContent(node)
      };
      const { children } = this.props;
      return (
        <WrappedComponent {...this.props} {...newProps}>
          {children}
        </WrappedComponent>
      );
    }
  };
};

export default withModal;
