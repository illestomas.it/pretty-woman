export interface FileTypes {
  file: string;
  height: number;
  source_url: string;
  width: number;
}

export interface Page {
  author: number;
  comment_status: string;
  content: {
    rendered: string;
    protected: boolean;
  };
  date: string;
  date_gmt: string;
  excerpt: {
    rendered: string;
    protected: boolean;
  };
  featured_media: number;
  guid: {
    rendered: string;
  };
  id: number;
  link: string;
  menu_order: number;
  meta: any[];
  modified: string;
  modified_gmt: string;
  parent: number;
  ping_status: string;
  slug: string;
  status: string;
  template: {
    rendered: string;
  };
  title: {
    rendered: string;
  };
  type: string;
  links: any;
}

export interface Tag {
  count: number;
  description: string;
  id: number;
  link: string;
  meta: any[];
  name: string;
  slug: string;
  taxonomy: string;
}

export interface Post {
  acf: boolean;
  author: number;
  better_featured_image: {
    alt_text: string;
    caption: string;
    description: string;
    id: number;
    media_details: {
      file: string;
      height: number;
      image_meta: {
        aperture: string;
        camera: string;
        caption: string;
        copyright: string;
        created_timestamp: string;
        credit: string;
        focal_length: string;
        iso: string;
        keywords: any[];
        orientation: string;
        shutter_speed: string;
        title: string;
      };
      sizes: {
        large: FileTypes;
        medium: FileTypes;
        medium_large: FileTypes;
        thumbnail: FileTypes;
      };
    };
    source_url: string;
  };
  categories: number[];
  comment_status: string;
  content: {
    rendered: string;
    protected: boolean;
  };
  date: string;
  date_gmt: string;
  excerpt: {
    rendered: string;
    protected: boolean;
  };
  featured_media: number;
  format: string;
  guid: { rendered: string };
  id: number;
  link: string;
  meta: any[];
  modified: string;
  modified_gmt: string;
  ping_status: string;
  slug: string;
  status: string;
  sticky: boolean;
  tags: number[];
  template: string;
  title: {
    rendered: string;
  };
  type: string;
  _links: any;
}
