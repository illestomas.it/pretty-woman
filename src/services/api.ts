import { Post, Page } from "./types";

interface QueryParameters {
  [key: string]: string | number | string[] | number[] | boolean | undefined;
}

export interface ApiTypes {
  setToken: (token: string) => void;
  setBaseUrl: (baseUrl: string) => void;
  serializeQueryParams: (params: QueryParameters) => string;
  request: (
    url: string,
    method: string,
    headers: Headers,
    body: any,
    queryParameters: QueryParameters
  ) => Promise<GlobalFetch>;
  getPages: (params: QueryParameters) => Promise<Page[]>;
  getPosts: (params: QueryParameters) => Promise<Post[]>;
  getMediaById: (id: number, params: QueryParameters) => Promise<any>;
  searchPosts: (params: QueryParameters) => Promise<Post[]>;
  getPostById: (id: number, params: QueryParameters) => Promise<Post>;
  getTags: (params: QueryParameters) => any;
}

class API implements ApiTypes {
  protected token: string;
  protected baseUrl: string;

  public setToken = (token: string) => {
    this.token = token;
  };

  public setBaseUrl = (baseUrl: string) => {
    this.baseUrl = `${baseUrl}/wp/v2`;
  };

  public serializeQueryParams = (parameters: QueryParameters) => {
    const str: string[] = [];
    for (const p in parameters) {
      if (parameters.hasOwnProperty(p)) {
        if (parameters[p] !== undefined) {
          let parameter = parameters[p];

          if (parameter === undefined || parameter === null) {
            parameter = "";
          }

          str.push(
            `${encodeURIComponent(p)}=${encodeURIComponent(
              parameter.toString()
            )}`
          );
        }
      }
    }
    return str.join("&");
  };

  public request = async (
    url: string,
    method: string,
    headers: Headers,
    body: any,
    queryParameters: QueryParameters
  ) => {
    const queryParams =
      queryParameters && Object.keys(queryParameters).length
        ? this.serializeQueryParams(queryParameters)
        : null;
    const urlWithParams = url + (queryParams ? "?" + queryParams : "");

    if (body && !Object.keys(body).length) {
      body = undefined;
    } else {
      body = JSON.stringify(body);
    }

    const response = await fetch(urlWithParams, { method, headers, body });
    if (response.ok) {
      return response.json();
    } else {
      console.log(response.statusText);
    }
  };

  public getPages = (params: QueryParameters = {}) => {
    const path = "pages";
    const headers: Headers = new Headers();

    headers.append("Accept", "application/json");
    headers.append("Content-Type", "application/json");
    return this.request(`${this.baseUrl}/${path}`, "GET", headers, {}, params);
  };

  public getPosts = (params: QueryParameters = {}) => {
    const path = "posts";
    const headers: Headers = new Headers();

    headers.append("Accept", "application/json");
    headers.append("Content-Type", "application/json");
    return this.request(`${this.baseUrl}/${path}`, "GET", headers, {}, params);
  };

  public getMediaById = (id: number, params: QueryParameters = {}) => {
    const path = `media/${id}`;
    const headers: Headers = new Headers();

    headers.append("Accept", "application/json");
    headers.append("Content-Type", "application/json");
    return this.request(`${this.baseUrl}/${path}`, "GET", headers, {}, params);
  };

  public searchPosts = (params: QueryParameters = {}) => {
    const path = "posts";
    const headers: Headers = new Headers();
    headers.append("Accept", "application/json");
    headers.append("Content-Type", "application/json");

    return this.request(`${this.baseUrl}/${path}`, "GET", headers, {}, params);
  };

  public getPostById = (id: number, params: QueryParameters = {}) => {
    const path = `posts/${id}`;
    const headers: Headers = new Headers();
    headers.append("Accept", "application/json");
    headers.append("Content-Type", "application/json");
    return this.request(`${this.baseUrl}/${path}`, "GET", headers, {}, params);
  };

  public getTags = (params: QueryParameters = {}) => {
    const path = "tags";
    const headers: Headers = new Headers();
    headers.append("Accept", "application/json");
    headers.append("Content-Type", "application/json");
    return this.request(`${this.baseUrl}/${path}`, "GET", headers, {}, params);
  };
}

export default new API();
