import * as wpapi from "wpapi";
import api from "wordpress-rest-api-oauth-2";

export const oauthApi = new api({
  brokerCredentials: {
    client: {
      id: process.env.REACT_APP_WP_CLIENT_ID,
      secret: process.env.REACT_APP_WP_CLIENT_SECRET
    }
  },
  callbackURL: process.env.REACT_APP_BASE_URL,
  credentials: {
    client: {
      id: process.env.REACT_APP_WP_CLIENT_ID,
      secret: process.env.REACT_APP_WP_CLIENT_SECRET
    }
  },
  url: process.env.REACT_APP_BASE_URL
});

export const wp = new wpapi({
  auth: true,
  endpoint: process.env.REACT_APP_WP_API_BASE_URL,
  password: process.env.REACT_APP_API_USERNAME,
  username: process.env.REACT_APP_WP_API_PASSWORD
});
