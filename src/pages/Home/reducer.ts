import * as actionTypes from "./constants";
import { AnyAction } from "redux";

const homeSliderPostsnitialState = {
  error: null,
  ids: [],
  isFetching: false
};

const homeSliderPostsReducer = (
  state = homeSliderPostsnitialState,
  action: AnyAction
) => {
  switch (action.type) {
    case actionTypes.REQUEST_HOME_POSTS: {
      return {
        ...state,
        isFetching: true
      };
    }
    case actionTypes.RECEIVE_HOME_POSTS_SUCCESS: {
      return {
        ...state,
        ids: action.payload.ids,
        isFetching: false
      };
    }
    case actionTypes.RECEIVE_HOME_POSTS_FAILURE: {
      return {
        ...state,
        error: action.payload.error,
        isFetching: false
      };
    }
    default:
      return state;
  }
};

export { homeSliderPostsReducer };
