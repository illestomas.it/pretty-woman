import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import * as actionTypes from "./constants";
import { State } from "src/store/rootReducer";
import { normalize } from "normalizr";
import { PostsSchema } from "src/schemas";
import { ApiTypes } from "src/services/api";

const requestHomePosts = () => ({
  type: actionTypes.REQUEST_HOME_POSTS
});

const receiveHomePostsSuccess = (ids: number[], entities: any) => ({
  payload: {
    entities,
    ids
  },
  type: actionTypes.RECEIVE_HOME_POSTS_SUCCESS
});

const receiveHomePostsFailure = (error: any) => ({
  payload: {
    error
  },
  type: actionTypes.RECEIVE_HOME_POSTS_FAILURE
});

export const getHomePosts = (limit = 3) => {
  return async (
    dispatch: ThunkDispatch<{}, {}, AnyAction>,
    getState: () => State,
    API: ApiTypes
  ) => {
    dispatch(requestHomePosts());
    try {
      const data = await API.getPosts({
        per_page: limit,
        sticky: true
      });
      const normalized = normalize(data, [PostsSchema]);
      dispatch(receiveHomePostsSuccess(normalized.result, normalized.entities));
    } catch (e) {
      dispatch(receiveHomePostsFailure(e));
    }
  };
};
