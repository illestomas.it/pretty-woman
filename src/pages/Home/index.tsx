import * as React from "react";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import styled from "src/theme/styledComponents";
import { rem } from "polished";
import { getHomePosts } from "./actions";
import { connect } from "react-redux";
import { homeSliderPostsSelector } from "./selectors";
import { State } from "src/store/rootReducer";
import { Post } from "src/services/types";
import Slider from "src/components/Slider";

interface Props {
  dispatch: ThunkDispatch<{}, {}, AnyAction>;
  sliderPosts: Post[];
}

const StyledSlider = styled(Slider)`
  height: ${rem(500)};
  & .slider {
    height: 100% !important;
  }

  & .slider-frame {
    height: 100% !important;
  }

  & .slider-frame ul,
  .slider-frame ul li {
    height: 100% !important;
  }
`;

class Home extends React.PureComponent<Props> {
  public componentDidMount() {
    const { dispatch } = this.props;
    dispatch(getHomePosts());
  }

  public render() {
    const { sliderPosts } = this.props;
    return <StyledSlider posts={sliderPosts} />;
  }
}

const mapStateToProps = (state: State) => ({
  sliderPosts: homeSliderPostsSelector(state)
});

export default connect(mapStateToProps)(Home);
