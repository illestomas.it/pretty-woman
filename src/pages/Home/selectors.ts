import { createSelector } from "reselect";
import { State } from "src/store/rootReducer";
import { entitiesTypeSelector } from "../App/selectors";

const homeSliderPostsDomain = (state: State) => state.homeSliderPosts;

const homeSliderIdsSelector = createSelector(
  homeSliderPostsDomain,
  home => home.ids
);

export const homeSliderPostsSelector = createSelector(
  entitiesTypeSelector("posts"),
  homeSliderIdsSelector,
  (entities, ids) => ids.map((id: number) => entities[id])
);
