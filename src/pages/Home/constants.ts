const namespace = "Home";

export const REQUEST_HOME_POSTS = `${namespace}/REQUEST_HOME_POSTS`;
export const RECEIVE_HOME_POSTS_SUCCESS = `${namespace}/RECEIVE_HOME_POSTS_SUCCESS`;
export const RECEIVE_HOME_POSTS_FAILURE = `${namespace}/RECEIVE_HOME_POSTS_FAILURE`;
