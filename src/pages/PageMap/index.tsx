import * as React from "react";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { getPage } from "./actions";
import Posts from "../Posts";
import Page from "src/pages/Page";
import { currentPageSelector } from "./selectors";
import { State } from "src/store/rootReducer";
import { Page as PageType } from "src/services/types";
import Home from "../Home";
import { getProp } from "src/utilities";

interface Props {
  match: {
    params: {
      slug: string;
    };
  };
  dispatch: ThunkDispatch<{}, {}, AnyAction>;
  currentPage: PageType;
}

class PageMap extends React.Component<Props> {
  public componentDidMount() {
    const {
      match: {
        params: { slug }
      },
      dispatch
    } = this.props;
    dispatch(getPage(slug));
  }

  public componentWillReceiveProps(nextProps: Props) {
    const {
      match: {
        params: { slug: nextSlug }
      },
      dispatch
    } = nextProps;
    if (this.props.match.params.slug !== nextSlug) {
      dispatch(getPage(nextSlug));
    }
  }

  public render() {
    const {
      match: {
        params: { slug }
      },
      currentPage
    } = this.props;
    const current = getProp(currentPage, "link", "");
    let page;
    if (slug === "blog") {
      page = <Posts />;
    } else if (!current.includes(currentPage.slug)) {
      page = <Home />;
    } else {
      page = <Page />;
    }

    return page;
  }
}

const mapStateToProps = (state: State) => ({
  currentPage: currentPageSelector(state)
});

export default connect(mapStateToProps)(PageMap);
