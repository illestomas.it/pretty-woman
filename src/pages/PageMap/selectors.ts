import { createSelector } from "reselect";
import { State } from "src/store/rootReducer";
import { entitiesTypeSelector } from "src/pages/App/selectors";

export const currentPageIdSelector = (state: State) => state.currentPageId;

export const currentPageSelector = createSelector(
  currentPageIdSelector,
  entitiesTypeSelector("pages"),
  (id, pages) => pages[id] || {}
);
