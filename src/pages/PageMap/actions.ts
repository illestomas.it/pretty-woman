import { AnyAction, Dispatch } from "redux";
import * as actionTypes from "./constants";
import { State } from "src/store/rootReducer";

const setCurrentPageId = (id: number) => ({
  payload: {
    id
  },
  type: actionTypes.SET_CURRENT_PAGE_ID
});

export const getPage = (slug: string) => {
  return async (
    dispatch: Dispatch<AnyAction>,
    getState: () => State,
    API: any
  ) => {
    try {
      const data = await API.getPages({
        slug
      });
      if (data.length > 0) {
        dispatch(setCurrentPageId(data[0].id));
      }
    } catch (e) {
      console.log(e, "page error");
    }
  };
};
