const namespace = "Page";

export const SET_CURRENT_PAGE_ID = `${namespace}/SET_CURRENT_PAGE_ID`;
