import { AnyAction } from "redux";
import * as actionTypes from "./constants";

export const currentPageIdreducer = (state = null, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.SET_CURRENT_PAGE_ID: {
      return action.payload.id;
    }
    default:
      return state;
  }
};
