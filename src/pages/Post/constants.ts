const namespace = "Post";

// post

export const REQUEST_POST = `${namespace}/REQUEST_POST`;
export const RECEIVE_POST_SUCCESS = `${namespace}/RECEIVE_POST_SUCCESS`;
export const RECEIVE_POST_FAILURE = `${namespace}/RECEIVE_POST_FAILURE`;

// current post id

export const SET_CURRENT_POST_ID = `${namespace}/SET_CURRENT_POST_ID`;

// related articles

export const REQUEST_RELATED_ARTICLES = `${namespace}/REQUEST_RELATED_ARTICLES`;
export const RECEIVE_RELATED_ARTICLES_SUCCESS = `${namespace}/RECEIVE_RELATED_ARTICLES_SUCCESS`;
export const RECEIVE_RELATED_ARTICLES_FAILURE = `${namespace}/RECEIVE_RELATED_ARTICLES_FAILURE`;
