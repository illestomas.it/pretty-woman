import * as React from "react";
import { connect } from "react-redux";
import { Post as PostType, Tag as TagType } from "src/services/types";
import { getPost, setCurrentPostId, getRelatedArticles } from "./actions";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { currentPostSelector, relatedArticlesSelector } from "./selectors";
import { State } from "src/store/rootReducer";
import { Container } from "src/components/Layout";
import PostHeader from "src/components/Post/Header";
import { tagsSelector } from "../Posts/selectors";
import { getTags } from "../Posts/actions";
import styled from "src/theme/styledComponents";
import { rem } from "polished";
import TagsList from "src/containers/TagsList";
import { getCurrentTags } from "src/utilities";
import PostsList from "src/containers/PostsList";

interface Props {
  match: {
    params: {
      id: string;
    };
  };
  dispatch: ThunkDispatch<{}, {}, AnyAction>;
  currentPost: PostType;
  tags: TagType[];
  relatedArticles: PostType[];
}

const Wrapper = styled.div`
  display: flex;
`;

const Body = styled.div`
  font-size: ${rem(18)};
  line-height: 1.5;

  & figure {
    margin: 0;
  }
`;

const StyledTagsList = styled(TagsList)`
  margin-top: ${rem(10)};
`;

class Post extends React.Component<Props> {
  public componentDidMount() {
    const {
      match: {
        params: { id }
      },
      dispatch
    } = this.props;
    const postId = Number(id);
    dispatch(setCurrentPostId(postId));
    dispatch(getPost(postId));
    dispatch(getTags());
    dispatch(getRelatedArticles());
  }

  public componentWillReceiveProps(nextProps: Props) {
    const {
      match: {
        params: { id: nextId }
      }
    } = nextProps;
    if (nextId !== this.props.match.params.id) {
      const nextPostId = Number(nextId);
      nextProps.dispatch(setCurrentPostId(nextPostId));
      nextProps.dispatch(getPost(nextPostId));
      nextProps.dispatch(getRelatedArticles());
    }
  }

  public render() {
    const { currentPost, tags, relatedArticles } = this.props;
    console.log(getCurrentTags(tags, currentPost));
    const body = currentPost.content ? currentPost.content.rendered : "";
    return (
      <Wrapper>
        <Container>
          <PostHeader post={currentPost} tags={tags} />
          <StyledTagsList tags={getCurrentTags(tags, currentPost)} />
          <Body dangerouslySetInnerHTML={{ __html: body }} />
          <PostsList
            posts={relatedArticles}
            tags={tags}
            title="További bejegyzések"
          />
        </Container>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state: State) => ({
  currentPost: currentPostSelector(state),
  relatedArticles: relatedArticlesSelector(state),
  tags: tagsSelector(state)
});

export default connect(mapStateToProps)(Post);
