import { AnyAction } from "redux";
import * as actionTypes from "./constants";

export const currentPostIdReducer = (state = null, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.SET_CURRENT_POST_ID: {
      return action.payload.id;
    }
    default:
      return state;
  }
};

const relatedArticlesInitialState = {
  ids: [],
  isFetching: false
};

export const relatedArticlesReducer = (
  state = relatedArticlesInitialState,
  action: AnyAction
) => {
  switch (action.type) {
    case actionTypes.REQUEST_RELATED_ARTICLES: {
      return {
        ...state,
        isFetching: true
      };
    }
    case actionTypes.RECEIVE_RELATED_ARTICLES_SUCCESS: {
      return {
        ...state,
        ids: action.payload.ids,
        isFetching: false
      };
    }
    case actionTypes.RECEIVE_RELATED_ARTICLES_FAILURE: {
      return {
        ...state,
        error: action.payload.error
      };
    }

    default:
      return state;
  }
};
