import { ApiTypes } from "src/services/api";
import { State } from "src/store/rootReducer";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import * as actionTypes from "./constants";
import { normalize } from "normalizr";
import { PostsSchema } from "src/schemas";

const requestPost = () => ({
  type: actionTypes.REQUEST_POST
});

const receivePostSucccess = (ids: number[], entities: any) => ({
  payload: {
    entities,
    ids
  },
  type: actionTypes.RECEIVE_POST_SUCCESS
});

const receivePostFailure = (error: any) => ({
  payload: {
    error
  },
  type: actionTypes.RECEIVE_POST_FAILURE
});

export const getPost = (id: number) => {
  return async (
    dispatch: ThunkDispatch<{}, {}, AnyAction>,
    getState: () => State,
    API: ApiTypes
  ) => {
    dispatch(requestPost());
    try {
      const req = await API.getPostById(id, {});
      const normalized = normalize(req, PostsSchema);
      dispatch(receivePostSucccess(normalized.result, normalized.entities));
    } catch (e) {
      dispatch(receivePostFailure(e));
    }
  };
};

export const setCurrentPostId = (id: number) => ({
  payload: {
    id
  },
  type: actionTypes.SET_CURRENT_POST_ID
});

// related articles action

const requestRelatedArticles = () => ({
  type: actionTypes.REQUEST_RELATED_ARTICLES
});

const receiveRelatedArticlesSuccess = (ids: number[], entities: any) => ({
  payload: {
    entities,
    ids
  },
  type: actionTypes.RECEIVE_RELATED_ARTICLES_SUCCESS
});

const receiveRelatedArticlesFailure = (error: any) => ({
  payload: {
    error
  },
  type: actionTypes.RECEIVE_RELATED_ARTICLES_FAILURE
});
export const getRelatedArticles = () => {
  return async (
    dispatch: ThunkDispatch<{}, {}, AnyAction>,
    getState: () => State,
    API: ApiTypes
  ) => {
    dispatch(requestRelatedArticles());
    try {
      const current = getState().currentPostId;
      const res = await API.getPosts({
        exclude: current,
        per_page: 3
      });
      const normalized = normalize(res, [PostsSchema]);
      dispatch(
        receiveRelatedArticlesSuccess(normalized.result, normalized.entities)
      );
    } catch (e) {
      dispatch(receiveRelatedArticlesFailure(e));
    }
  };
};
