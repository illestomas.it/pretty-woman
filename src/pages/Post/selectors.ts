import { createSelector } from "reselect";
import { State } from "src/store/rootReducer";
import { entitiesTypeSelector } from "../App/selectors";

const currentPostIdSelector = (state: State) => state.currentPostId;
const relatedArticlesDomain = (state: State) => state.relatedArticles;

export const currentPostSelector = createSelector(
  currentPostIdSelector,
  entitiesTypeSelector("posts"),
  (id, posts) => posts[id] || {}
);

const relatedArticlesIdsSelector = createSelector(
  relatedArticlesDomain,
  relatedArticles => relatedArticles.ids
);

export const relatedArticlesSelector = createSelector(
  entitiesTypeSelector("posts"),
  relatedArticlesIdsSelector,
  (posts, ids) => ids.map((id: number) => posts[id])
);
