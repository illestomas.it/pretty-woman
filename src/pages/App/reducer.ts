import { AnyAction } from "redux";
import * as actionTypes from "./constants";

const entitiesInitialState = {
  pages: {},
  posts: {},
  tags: {}
};

const pagesInitialState = {
  isFetching: false,
  pages: []
};

const entitiesReducer = (state = entitiesInitialState, action: AnyAction) => {
  if (action.payload && action.payload.entities) {
    const { entities } = action.payload;
    if ("posts" in entities) {
      return {
        ...state,
        posts: {
          ...state.posts,
          ...action.payload.entities.posts
        }
      };
    }
    if ("pages" in entities) {
      return {
        ...state,
        pages: {
          ...state.pages,
          ...action.payload.entities.pages
        }
      };
    }
    if ("tags" in entities) {
      return {
        ...state,
        tags: {
          ...state.tags,
          ...action.payload.entities.tags
        }
      };
    }
  }
  return state;
};

const pagesReducer = (state = pagesInitialState, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.REQUEST_PAGES: {
      return {
        ...state,
        isFetching: true
      };
    }
    case actionTypes.RECEIVE_PAGES_SUCCESS: {
      return {
        ...state,
        isFetching: false,
        pages: action.payload.ids
      };
    }
    case actionTypes.RECEIVE_PAGES_FAILURE: {
      return {
        ...state,
        error: action.payload.e,
        isFetching: false
      };
    }
    default:
      return state;
  }
};

const authInitialState = {
  user: ""
};

const authReducer = (state = authInitialState, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.SET_TOKEN: {
      return {
        token: action.payload.token
      };
    }
    default:
      return state;
  }
};

const searchInitialState = {
  isFetching: false,
  query: "",
  resultsByQuery: {}
};

const searchReducer = (state = searchInitialState, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.SET_SEARCH_QUERY: {
      return {
        ...state,
        query: action.payload.query
      };
    }
    case actionTypes.REQUEST_SEARCH: {
      return {
        ...state,
        isFetching: true
      };
    }
    case actionTypes.RECEIVE_SEARCH_SUCCESS: {
      return {
        ...state,
        isFetching: false,
        resultsByQuery: {
          ...state.resultsByQuery,
          [action.payload.query]: action.payload.ids
        }
      };
    }
    case actionTypes.RECEIVE_MEDIA_BY_ID_FAILURE: {
      return {
        ...state,
        error: action.payload.error,
        isFetching: false
      };
    }
    default:
      return state;
  }
};

const modalInitialState = {
  body: null,
  visible: false
};

const modalReducer = (state = modalInitialState, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.OPEN_MODAL: {
      return {
        ...state,
        visible: true
      };
    }
    case actionTypes.CLOSE_MODAL: {
      return {
        ...state,
        visible: false
      };
    }
    case actionTypes.SET_MODAL_BODY: {
      return {
        ...state,
        body: action.payload.node
      };
    }
    default:
      return state;
  }
};

export {
  entitiesReducer,
  pagesReducer,
  authReducer,
  searchReducer,
  modalReducer
};
