import * as React from "react";
import * as Cookies from "js-cookie";
import { Dispatch } from "redux";
import { connect } from "react-redux";
import { ThemeInterface } from "src/theme/types";
import { renderRoutes } from "react-router-config";
import Header from "src/containers/Header";
import styled, { GlobalStyle } from "src/theme/styledComponents";
import {
  loadPages,
  setToken,
  setSearchQuery,
  search as initSearch
} from "./actions";
import {
  pagesSelector,
  searchQuerySelector,
  searchResultsSelector,
  modalVisibleSelector,
  modalBodySelector
} from "./selectors";
import { State as ReduxState } from "src/store/rootReducer";
import { Page as PageType } from "src/services/types";
import { parse } from "query-string";
import { Post } from "src/services/types";
import withModal from "src/hoc/Modal";
import Modal from "src/components/Modal";
import Search from "src/components/Search";
import Sidebar from "src/components/Sidebar";
import Menu from "src/components/Menu";

interface Props {
  theme: ThemeInterface;
  route: any;
  dispatch: Dispatch<any>;
  pages: PageType[];
  history: History;
  location: {
    search: string;
  };
  query: string;
  searchResults: Post[];
  handleCloseModal: () => void;
  handleOpenModal: () => void;
  modalVisible: boolean;
  modalBody: React.ReactNode;
}

interface State {
  sidebarOpened: boolean;
}

const Wrapper = styled.div`
  position: relative;
`;

class App extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      sidebarOpened: false
    };
  }

  public componentDidMount() {
    const { dispatch } = this.props;
    dispatch(loadPages());
    this.getToken();
  }

  public componentWillReceiveProps(nextProps: Props) {
    if (
      nextProps.location !== this.props.location &&
      this.state.sidebarOpened
    ) {
      this.handleToggleSidebar();
    }
  }
  public getToken = async () => {
    const {
      location: { search },
      dispatch
    } = this.props;
    const authToken = Cookies.get("authToken");
    if (!authToken) {
      const req = await fetch(
        `${process.env.REACT_APP_BASE_URL}oauth/authorize/?client_id=${
          process.env.REACT_APP_WP_CLIENT_ID
        }&response_type=code`,
        { method: "GET" }
      );
      const parsed = parse(search);
      if (!search.includes("?code=")) {
        window.location.href = req.url;
      } else {
        const tokenReq = await fetch(
          `${process.env.REACT_APP_BASE_URL}oauth/token`,
          {
            body: `client_id=${
              process.env.REACT_APP_WP_CLIENT_ID
            }&client_secret=${process.env.REACT_APP_WP_CLIENT_SECRET}&code=${
              parsed.code
            }&grant_type=authorization_code`,
            headers: new Headers({
              "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
            }),
            method: "POST"
          }
        );
        const data = await tokenReq.json();
        Cookies.set("authToken", data.access_token);
      }
    } else {
      dispatch(setToken(authToken));
    }
  };

  public handleChangeQuery = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { dispatch } = this.props;
    dispatch(setSearchQuery(event.target.value));
    dispatch(initSearch());
  };

  public handleToggleSidebar = () => {
    console.log("clicked");
    this.setState(prevState => ({ sidebarOpened: !prevState.sidebarOpened }));
  };

  public render() {
    const {
      route: { routes },
      pages,
      query,
      searchResults,
      handleOpenModal,
      handleCloseModal,
      modalVisible
    } = this.props;
    const { sidebarOpened } = this.state;
    return (
      <Wrapper>
        <GlobalStyle />
        <Sidebar isOpened={sidebarOpened}>
          <Menu pages={pages} />
        </Sidebar>
        <Modal
          handleOpenModal={handleOpenModal}
          handleCloseModal={handleCloseModal}
          visible={modalVisible}
        >
          <Search
            handleChangeQuery={this.handleChangeQuery}
            query={query}
            searchResults={searchResults}
          />
        </Modal>
        <Header
          pages={pages}
          handleChangeQuery={this.handleChangeQuery}
          query={query}
          searchResults={searchResults}
          handleOpenModal={handleOpenModal}
          handleOpenSidebar={this.handleToggleSidebar}
          sidebarOpened={sidebarOpened}
        />
        {renderRoutes(routes)}
        <div>foot</div>
      </Wrapper>
    );
  }
}

const mapStateToProps = (state: ReduxState) => ({
  modalBody: modalBodySelector(state),
  modalVisible: modalVisibleSelector(state),
  pages: pagesSelector(state),
  query: searchQuerySelector(state),
  searchResults: searchResultsSelector(state)
});

const withModalProps = withModal(App);

export default connect(mapStateToProps)(withModalProps);
