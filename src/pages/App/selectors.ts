import { createSelector } from "reselect";
import { State } from "src/store/rootReducer";

const pagesDomain = (state: State) => state.pages;
const entitiesDomain = (state: State) => state.entities;
const searchDomain = (state: State) => state.search;
const modalDomain = (state: State) => state.modal;

export const entitiesTypeSelector = (type: string) =>
  createSelector(
    entitiesDomain,
    entities => entities[type] || {}
  );

export const pagesIdsSelector = createSelector(
  pagesDomain,
  substate => substate.pages || []
);

export const pagesSelector = createSelector(
  pagesIdsSelector,
  entitiesTypeSelector("pages"),
  (ids, pages) => ids.map((id: number) => pages[id])
);

export const searchQuerySelector = createSelector(
  searchDomain,
  search => search.query
);

export const resultsByQueryDomain = createSelector(
  searchDomain,
  search => search.resultsByQuery || {}
);

export const searchResultsIdsByQuerySelector = createSelector(
  searchQuerySelector,
  resultsByQueryDomain,
  (query, byQuery) => byQuery[query] || []
);

export const searchResultsSelector = createSelector(
  searchResultsIdsByQuerySelector,
  entitiesTypeSelector("posts"),
  (ids, posts) => ids.map((id: number) => posts[id])
);

export const modalVisibleSelector = createSelector(
  modalDomain,
  modal => modal.visible
);

export const modalBodySelector = createSelector(
  modalDomain,
  modal => modal.body
);
