const namespace = "App";

export const REQUEST_PAGES = `${namespace}/REQUEST_PAGES`;
export const RECEIVE_PAGES_SUCCESS = `${namespace}/RECEIVE_PAGES_SUCCESS`;
export const RECEIVE_PAGES_FAILURE = `${namespace}/RECEIVE_PAGES_FAILURE`;

// token

export const SET_TOKEN = `${namespace}/SET_TOKEN`;

// media

export const REQUEST_MEDIA_BY_ID = `${namespace}/REQUEST_MEDIA_BY_ID`;
export const RECEIVE_MEDIA_BY_ID_SUCCESS = `${namespace}/RECEIVE_MEDIA_BY_ID_SUCCESS`;
export const RECEIVE_MEDIA_BY_ID_FAILURE = `${namespace}/RECEIVE_MEDIA_BY_ID_FAILURE`;

// setting search query

export const SET_SEARCH_QUERY = `${namespace}/SET_SEARCH_QUERY`;

// search

export const REQUEST_SEARCH = `${namespace}/REQUEST_SEARCH`;
export const RECEIVE_SEARCH_SUCCESS = `${namespace}/RECEIVE_SEARCH_SUCCESS`;
export const RECEIVE_SEARCH_FAILURE = `${namespace}/RECEIVE_SEARCH_FAILURE`;

// modal

export const OPEN_MODAL = `${namespace}/OPEN_MODAL`;
export const CLOSE_MODAL = `${namespace}/CLOSE_MODAL`;
export const SET_MODAL_BODY = `${namespace}/SET_MODAL_BODY`;
