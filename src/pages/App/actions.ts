import { State } from "src/store/rootReducer";
import { Dispatch, AnyAction } from "redux";
import { normalize } from "normalizr";
import * as actionTypes from "./constants";
import { PagesSchema, PostsSchema } from "src/schemas";
import { ApiTypes } from "src/services/api";
import { SEARCH_QUERY_LIMIT } from "src/components/Search";

const requestPages = () => ({
  type: actionTypes.REQUEST_PAGES
});

const receivePagesSuccess = (ids: number[], entities: object) => ({
  payload: {
    entities,
    ids
  },
  type: actionTypes.RECEIVE_PAGES_SUCCESS
});

const receivePagesFailure = (e: any) => ({
  payload: {
    e
  },
  type: actionTypes.RECEIVE_PAGES_FAILURE
});

export const setToken = (token: string) => ({
  payload: {
    token
  },
  type: actionTypes.SET_TOKEN
});

export const loadPages = () => {
  return async (
    dispatch: Dispatch<AnyAction>,
    getState: () => State,
    API: any
  ) => {
    dispatch(requestPages());
    try {
      const pages = await API.getPages({ order: "asc", orderby: "menu_order" });
      const normalized = normalize(pages, [PagesSchema]);
      dispatch(receivePagesSuccess(normalized.result, normalized.entities));
    } catch (e) {
      console.log(e);
      dispatch(receivePagesFailure(e));
    }
  };
};

export const setSearchQuery = (query: string) => ({
  payload: {
    query
  },
  type: actionTypes.SET_SEARCH_QUERY
});

const requestSearch = () => ({
  type: actionTypes.REQUEST_SEARCH
});

const receiveSearchSuccess = (
  ids: number[],
  entities: object,
  query: string
) => ({
  payload: {
    entities,
    ids,
    query
  },
  type: actionTypes.RECEIVE_SEARCH_SUCCESS
});

const receiveSearchFailure = (error: any) => ({
  payload: {
    error
  },
  type: actionTypes.RECEIVE_SEARCH_FAILURE
});

export const search = () => {
  return async (
    dispatch: Dispatch<AnyAction>,
    getState: () => State,
    API: ApiTypes
  ) => {
    const query = getState().search.query;
    if (query.length >= SEARCH_QUERY_LIMIT) {
      dispatch(requestSearch());
      try {
        const results = await API.searchPosts({ search: query });
        const normalized = normalize(results, [PostsSchema]);
        dispatch(
          receiveSearchSuccess(normalized.result, normalized.entities, query)
        );
      } catch (e) {
        dispatch(receiveSearchFailure(e));
      }
    }
  };
};

export const openModal = () => ({
  type: actionTypes.OPEN_MODAL
});

export const closeModal = () => ({
  type: actionTypes.CLOSE_MODAL
});

export const setModalBody = (node: React.ReactNode) => ({
  payload: {
    node
  },
  type: actionTypes.SET_MODAL_BODY
});
