import * as actionTypes from "./constants";
import { normalize } from "normalizr";
import { PostsSchema, TagsSchema } from "../../schemas";
import { State } from "src/store/rootReducer";
import { AnyAction } from "redux";
import { ThunkDispatch } from "redux-thunk";
import { ApiTypes } from "src/services/api";

const requestPosts = () => ({
  type: actionTypes.REQUEST_POSTS
});

const receivePostsSucccess = (
  ids: number[],
  entities: any,
  offset: number
) => ({
  payload: { entities, ids, offset },
  type: actionTypes.RECEIVE_POSTS_SUCCESS
});

const receivePostsFailure = (error: any) => ({
  payload: {
    error
  },
  type: actionTypes.RECEIVE_POSTS_FAILURE
});

export const loadPosts = (
  limit: number = actionTypes.POSTS_PER_PAGE,
  offset: number = 0
) => {
  return async (
    dispatch: ThunkDispatch<{}, {}, AnyAction>,
    getState: () => State,
    API: ApiTypes
  ) => {
    dispatch(requestPosts());
    try {
      const response = await API.getPosts({
        offset,
        per_page: limit
      });
      const normalized = normalize(response, [PostsSchema]);
      console.log(normalized.result);
      dispatch(
        receivePostsSucccess(normalized.result, normalized.entities, offset)
      );
    } catch (e) {
      dispatch(receivePostsFailure(e));
    }
  };
};

const requestTagsByPostId = () => ({
  type: actionTypes.REQUEST_TAGS_BY_POST_ID
});

const receiveTagsByPostIdSuccess = (ids: number[], entities: any) => ({
  payload: {
    entities,
    ids
  },
  type: actionTypes.RECEIVE_TAGS_BY_POST_ID_SUCCESS
});

const receiveTagsByPostIdFailure = (error: any) => ({
  payload: {
    error
  },
  type: actionTypes.RECEIVE_TAGS_BY_POST_ID_FAILURE
});

export const getTags = () => {
  return async (
    dispatch: ThunkDispatch<{}, {}, AnyAction>,
    getState: () => State,
    API: ApiTypes
  ) => {
    dispatch(requestTagsByPostId());
    try {
      const response = await API.getTags({});
      const normalized = normalize(response, [TagsSchema]);
      dispatch(
        receiveTagsByPostIdSuccess(normalized.result, normalized.entities)
      );
    } catch (e) {
      dispatch(receiveTagsByPostIdFailure(e));
    }
  };
};
