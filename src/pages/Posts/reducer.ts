import * as actionTypes from "./constants";
import { AnyAction } from "redux";

const postsInitialState = {
  hasMore: true,
  ids: [],
  isFetching: false
};

const postsReducer = (state = postsInitialState, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.REQUEST_POSTS: {
      return {
        ...state,
        isFetching: true
      };
    }
    case actionTypes.RECEIVE_POSTS_SUCCESS: {
      return {
        ...state,
        hasMore: action.payload.ids.length === 0 ? false : true,
        ids:
          action.payload.offset === 0
            ? action.payload.ids
            : [...state.ids, ...action.payload.ids],
        isFetching: false
      };
    }
    case actionTypes.RECEIVE_POSTS_FAILURE: {
      return {
        ...state,
        error: action.payload.error,
        isFetching: false
      };
    }
    default:
      return state;
  }
};

const tagsInitialState = {
  error: null,
  ids: [],
  isFetching: false
};

const tagsReducer = (state = tagsInitialState, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.REQUEST_TAGS_BY_POST_ID: {
      return {
        ...state,
        isFetching: true
      };
    }
    case actionTypes.RECEIVE_TAGS_BY_POST_ID_SUCCESS: {
      return {
        ...state,
        ids: action.payload.ids,
        isFetching: false
      };
    }
    case actionTypes.RECEIVE_TAGS_BY_POST_ID_FAILURE: {
      return {
        ...state,
        error: action.payload.error,
        isFetching: false
      };
    }
    default:
      return state;
  }
};

export { postsReducer, tagsReducer };
