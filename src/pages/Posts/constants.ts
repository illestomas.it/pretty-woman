const namespace = "Posts";

// posts

export const REQUEST_POSTS = `${namespace}/REQUEST_POSTS`;
export const RECEIVE_POSTS_SUCCESS = `${namespace}/RECEIVE_POSTS_SUCCESS`;
export const RECEIVE_POSTS_FAILURE = `${namespace}/RECEIVE_POSTS_FAILURE`;

export const POSTS_PER_PAGE = 6;

// tags

export const REQUEST_TAGS_BY_POST_ID = `${namespace}/REQUEST_TAGS_BY_POST_ID`;
export const RECEIVE_TAGS_BY_POST_ID_SUCCESS = `${namespace}/RECEIVE_TAGS_BY_POST_ID_SUCCESS`;
export const RECEIVE_TAGS_BY_POST_ID_FAILURE = `${namespace}/RECEIVE_TAGS_BY_POST_ID_FAILURE`;
