import { createSelector } from "reselect";
import { State } from "src/store/rootReducer";
import { entitiesTypeSelector } from "../App/selectors";
import { Post } from "src/services/types";

const postsIdsDomain = (state: State) => state.posts;
const tagIdsDomain = (state: State) => state.tags;

const postsIdsSelector = createSelector(
  postsIdsDomain,
  posts => posts.ids || []
);

const tagIdsSelector = createSelector(
  tagIdsDomain,
  tag => tag.ids
);

export const postsSelector = createSelector(
  postsIdsSelector,
  entitiesTypeSelector("posts"),
  (ids, posts) =>
    ids
      .map((id: number) => posts[id])
      .filter((post: Post) => post.status === "publish")
);

export const hasMorePostsSelector = createSelector(
  postsIdsDomain,
  post => post.hasMore
);

export const tagsSelector = createSelector(
  tagIdsSelector,
  entitiesTypeSelector("tags"),
  (ids, tags) => ids.map((id: number) => tags[id])
);
