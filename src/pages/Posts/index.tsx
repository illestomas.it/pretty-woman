import * as React from "react";
import { connect } from "react-redux";
import { POSTS_PER_PAGE } from "./constants";
import { loadPosts, getTags } from "./actions";
import { State as StateTypes } from "src/store/rootReducer";
import { postsSelector, tagsSelector, hasMorePostsSelector } from "./selectors";
import { Container } from "src/components/Layout";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction } from "redux";
import { Post as PostTypes, Tag } from "src/services/types";
import PostsList from "src/containers/PostsList";

interface Props {
  dispatch: ThunkDispatch<{}, {}, AnyAction>;
  posts: PostTypes[];
  tags: Tag[];
  hasMore: boolean;
}

interface State {
  nextOffset: number;
}

class Posts extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      nextOffset: POSTS_PER_PAGE
    };
  }
  public componentDidMount() {
    const { dispatch } = this.props;
    dispatch(loadPosts());
    dispatch(getTags());
  }

  public loadMorePosts = () => {
    const { dispatch, posts } = this.props;
    const { nextOffset } = this.state;
    console.log(posts);
    dispatch(loadPosts(POSTS_PER_PAGE, nextOffset));
    this.setState(prevState => ({
      nextOffset: prevState.nextOffset + POSTS_PER_PAGE
    }));
  };

  public render() {
    const { posts, tags, hasMore } = this.props;
    console.log(this.props.posts);
    return (
      <Container>
        <PostsList posts={posts} tags={tags} />
        {hasMore && <button onClick={this.loadMorePosts}>more</button>}
      </Container>
    );
  }
}

const mapStateToProps = (state: StateTypes) => ({
  hasMore: hasMorePostsSelector(state),
  posts: postsSelector(state),
  tags: tagsSelector(state)
});

export default connect(mapStateToProps)(Posts);
