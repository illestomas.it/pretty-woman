import * as React from "react";
import * as ReactDOM from "react-dom";
import registerServiceWorker from "./registerServiceWorker";
import { Provider } from "react-redux";
import { ThemeProvider } from "src/theme/styledComponents";
import configureStore from "./store";
import Routes from "src/routes";
import theme from "src/theme";
import API from "src/services/api";
import * as Cookies from "js-cookie";
import "moment/locale/hu";
import * as moment from "moment";
import "moment/locale/hu";

moment.locale("hu");

let state = {};
if (typeof window !== "undefined") {
  state = (window as any).__REDUX_STATE__;
}

API.setBaseUrl(process.env.REACT_APP_WP_API_BASE_URL!);
const token = Cookies.get("authToken");
if (token) {
  API.setToken(token);
}

const store = configureStore(state !== "{{SSR_INITIAL_STATE}}" ? state : {});

const AppRoutes = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Routes />
      </ThemeProvider>
    </Provider>
  );
};

ReactDOM.render(<AppRoutes />, document.getElementById("root") as HTMLElement);

registerServiceWorker();
