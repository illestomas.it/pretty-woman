import { Tag, Post } from "src/services/types";

export const getProp = (object: any, path: any, defaultValue: any = "") => {
  if (path === undefined || path === null) {
    return defaultValue;
  }
  let pathArray = path;
  if (typeof path === "string") {
    pathArray = path.split(".");
  }
  const val = pathArray.reduce((o: any, x: any) => {
    return typeof o === "undefined" || o === null ? defaultValue : o[x];
  }, object);
  if (typeof val === "undefined" || val === null) {
    return defaultValue;
  }
  return val;
};

export const getCurrentTags = (tags: Tag[], post: Post) => {
  if (post.tags) {
    return tags.filter(
      tag => post.tags.length > 0 && post.tags.includes(tag.id)
    );
  }
  return [];
};
