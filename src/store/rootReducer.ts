import { routerReducer } from "react-router-redux";
import {
  entitiesReducer,
  pagesReducer,
  authReducer,
  searchReducer,
  modalReducer
} from "src/pages/App/reducer";
import { combineReducers } from "redux";
import { postsReducer, tagsReducer } from "src/pages/Posts/reducer";
import { currentPageIdreducer } from "src/pages/PageMap/reducer";
import { homeSliderPostsReducer } from "src/pages/Home/reducer";
import {
  currentPostIdReducer,
  relatedArticlesReducer
} from "src/pages/Post/reducer";

const rootReducer = combineReducers({
  auth: authReducer,
  currentPageId: currentPageIdreducer,
  currentPostId: currentPostIdReducer,
  entities: entitiesReducer,
  homeSliderPosts: homeSliderPostsReducer,
  modal: modalReducer,
  pages: pagesReducer,
  posts: postsReducer,
  relatedArticles: relatedArticlesReducer,
  routing: routerReducer,
  search: searchReducer,
  tags: tagsReducer
});

export default rootReducer;

export type StateType<ReducerOrMap> = ReducerOrMap extends (
  ...args: any[]
) => any
  ? ReturnType<ReducerOrMap>
  : ReducerOrMap extends object
  ? { [K in keyof ReducerOrMap]: StateType<ReducerOrMap[K]> }
  : never;

export type State = StateType<typeof rootReducer>;
