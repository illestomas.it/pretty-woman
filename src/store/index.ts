import { createBrowserHistory } from "history";
import { routerMiddleware } from "react-router-redux";
import { AnyAction, applyMiddleware, compose, createStore } from "redux";
import thunkMiddleware, { ThunkMiddleware } from "redux-thunk";
import rootReducer from "./rootReducer";
import API from "src/services/api";

let composeEnhancers;
if (typeof window !== "undefined") {
  composeEnhancers =
    (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
} else {
  composeEnhancers = compose;
}

const history = createBrowserHistory();

const thunk: ThunkMiddleware<{}, AnyAction> = thunkMiddleware.withExtraArgument(
  API
);

const enhancer = composeEnhancers(
  applyMiddleware(routerMiddleware(history), thunk)
);

export default function configureStore(initialState?: object) {
  return createStore(rootReducer, initialState!, enhancer);
}
