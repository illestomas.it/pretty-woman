import * as React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { renderRoutes } from "react-router-config";
import App from "src/pages/App";
import Home from "src/pages/Home";
import PageMap from "src/pages/PageMap";
import Post from "src/pages/Post";

const Routes: React.SFC<{}> = () => {
  return <Router>{renderRoutes(routes)}</Router>;
};

export const routes = [
  {
    component: App,
    routes: [
      {
        component: Home,
        exact: true,
        path: "/"
      },
      {
        component: PageMap,
        exact: true,
        path: "/:slug"
      },
      {
        component: Post,
        path: "/post/:id"
      }
    ]
  }
];

export default Routes;
