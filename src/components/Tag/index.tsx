import * as React from "react";
import { Tag as TagType } from "src/services/types";
import styled, { withTheme } from "src/theme/styledComponents";
import { ThemeInterface } from "src/theme/types";
import { rem } from "polished";

interface Props {
  className?: string;
  tag: TagType;
  theme: ThemeInterface;
}

const Wrapper = styled.div`
  background: ${({ theme }) => theme.colors.tagBackground};
  padding: ${rem(5)};
  font-size: ${rem(12)};
`;

const Tag: React.StatelessComponent<Props> = ({ className, tag, theme }) => {
  return <Wrapper className={className}>{tag.name}</Wrapper>;
};

export default withTheme(Tag);
