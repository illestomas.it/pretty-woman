import styled from "src/theme/styledComponents";
import { rem } from "polished";

export const Container = styled.div`
  max-width: ${({ theme }) => rem(theme.sizes.sm)};
  margin: auto;
  padding: ${rem(20)};
  width: 100%;
`;
