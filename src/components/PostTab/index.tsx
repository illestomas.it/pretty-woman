import * as React from "react";
import { rem } from "polished";
import { Post, Tag as TagType } from "src/services/types";
import styled, { withTheme } from "src/theme/styledComponents";
import Image from "src/components/Image";
import Icon from "../Icon";
import { ThemeInterface } from "src/theme/types";
import * as moment from "moment";
import { Link } from "react-router-dom";
import TagsList from "src/containers/TagsList";
import { getCurrentTags } from "src/utilities";

interface Props {
  className?: string;
  post: Post;
  theme: ThemeInterface;
  tags: TagType[];
}

const Wrapper = styled.div`
  background: #fff;
  flex: 0 0 calc(33.33% - ${rem(15)});
  margin-right: ${rem(15)};
  margin-bottom: ${rem(15)};
  display: flex;
  flex-direction: column;
  color: ${({ theme }) => theme.colors.textPrimary};
  &:hover img {
    transform: scale(1.1);
    opacity: 0.4;
  }
`;

const StyledImage = styled(Image)`
  object-fit: cover;
  transition: all 0.3s;
`;

const ImgWrapper = styled(Link)`
  display: flex;
  width: 100%;
  height: ${rem(200)};
  overflow: hidden;
  background: ${({ theme }) => theme.colors.primary};
`;

const Meta = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${rem(15)};
  flex: 1;
`;

const Title = styled.div`
  font-weight: 800;
  font-size: ${rem(18)};
  margin-bottom: ${rem(10)};
  transition: color 0.3s;

  &:hover {
    color: ${({ theme }) => theme.colors.primary};
  }
`;

const Excerpt = styled.div`
  font-size: ${rem(14)};
  color: ${({ theme }) => theme.colors.textPrimary};
  line-height: 1.5;
`;

const DateWrapper = styled.div`
  display: flex;
`;

const Date = styled.div`
  font-size: ${rem(12)};
  color: ${({ theme }) => theme.colors.incativeTextColor};
  margin-left: ${rem(10)};
  font-style: italic;
`;

const StyledTagsList = styled(TagsList)`
  margin-top: auto;
`;

const PostTab: React.StatelessComponent<Props> = ({
  post,
  className,
  theme,
  tags
}) => {
  const imgUrl = post.better_featured_image
    ? post.better_featured_image.source_url
    : "";
  const title = post.title ? post.title.rendered : "";
  const excerpt = post.excerpt ? post.excerpt.rendered : "";
  const date = moment(post.date).format("LL");
  return (
    <Wrapper className={className}>
      <ImgWrapper to={`/post/${post.id}`}>
        <StyledImage src={imgUrl} />
      </ImgWrapper>
      <Meta>
        <Title>
          <Link to={`/post/${post.id}`}>{title}</Link>
        </Title>
        <DateWrapper>
          <Icon
            name="clock"
            width="15"
            height="15"
            color={theme.colors.incativeTextColor}
          />
          <Date>{date}</Date>
        </DateWrapper>
        <Excerpt dangerouslySetInnerHTML={{ __html: excerpt }} />
        <StyledTagsList tags={getCurrentTags(tags, post)} />
      </Meta>
    </Wrapper>
  );
};

export default withTheme(PostTab);
