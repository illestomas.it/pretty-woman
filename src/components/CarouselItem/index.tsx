import * as React from "react";
import styled from "src/theme/styledComponents";
import { Post } from "src/services/types";

interface Props {
  post: Post;
}

const Wrapper = styled("div")<{ img: string }>`
  background: url(${({ img }) => img});
  height: 100%;
  background-size: cover;
  background-position: 50% 50%;
`;

const CarouselItem: React.SFC<Props> = ({ post }) => (
  <Wrapper img={post.better_featured_image.source_url}>
    {post.title.rendered}
  </Wrapper>
);

export default CarouselItem;
