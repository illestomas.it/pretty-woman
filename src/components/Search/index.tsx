import * as React from "react";
import { rem } from "polished";
import styled from "src/theme/styledComponents";
import Input from "../Input";
import { Post } from "src/services/types";
import SearchResultsList from "./ResultsList";

export const SEARCH_QUERY_LIMIT = 3;

interface Props {
  handleChangeQuery: (event: React.ChangeEvent<HTMLInputElement>) => void;
  query: string;
  searchResults: Post[];
}

const Wrapper = styled("div")`
  background: #fff;
  padding: ${rem(15)};
`;

const Search: React.SFC<Props> = (props: Props) => {
  const { handleChangeQuery, query, searchResults } = props;
  return (
    <Wrapper>
      <Input
        value={query}
        handleChangeQuery={handleChangeQuery}
        query={query}
      />
      <SearchResultsList posts={searchResults} query={query} />
    </Wrapper>
  );
};

export default Search;
