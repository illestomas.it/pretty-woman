import * as React from "react";
import { rem } from "polished";
import { Post } from "src/services/types";
import styled from "src/theme/styledComponents";
import { Link } from "react-router-dom";

interface Props {
  post: Post;
}

const Wrapper = styled.div`
  display: flex;
  margin-bottom: ${rem(30)};
`;

const Info = styled.div`
  display: flex;
  flex-direction: column;
`;

const Title = styled.div`
  font-weight: bold;
`;

const Excerpt = styled.div`
  font-size: ${rem(13)};
`;

const Result: React.SFC<Props> = ({ post }) => {
  return (
    <Wrapper>
      <Info>
        <Link to={`/post/${post.id}`}>
          <Title>{post.title.rendered}</Title>
        </Link>
        <Excerpt dangerouslySetInnerHTML={{ __html: post.excerpt.rendered }} />
      </Info>
    </Wrapper>
  );
};

export default Result;
