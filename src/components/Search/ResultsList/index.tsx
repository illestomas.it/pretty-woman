import * as React from "react";
import { Post } from "src/services/types";
import Result from "../Result";
import { SEARCH_QUERY_LIMIT } from "../index";

interface Props {
  posts: Post[];
  query: string;
}

const SearchResultsList: React.SFC<Props> = ({ posts, query }) => {
  return (
    <React.Fragment>
      {posts.length > 0 &&
        query.length >= SEARCH_QUERY_LIMIT &&
        posts.map(post => <Result key={post.id} post={post} />)}
    </React.Fragment>
  );
};

export default SearchResultsList;
