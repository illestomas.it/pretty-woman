import * as React from "react";
import styled, { createGlobalStyle } from "styled-components";
import { rem } from "polished";

export interface Props {
  visible: boolean;
  handleCloseModal: () => void;
  handleOpenModal: () => void;
}

export interface State {
  largeModal: boolean;
}

const Wrapper = styled("div")<{ visible: boolean; isLarge: boolean }>`
  width: 100%;
  height: 100%;
  position: fixed;
  background: rgba(255, 255, 255, 0.7);
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  transition: all 0.3s;
  opacity: ${({ visible }) => (visible ? 1 : 0)};
  visibility: ${({ visible }) => (visible ? "visible" : "hidden")};
  overflow: ${({ isLarge }) => (isLarge ? "scroll" : "auto")};
  z-index: 500;
`;

const ModalContent = styled("div")<{
  visible: boolean;
}>`
  margin: 0 auto;
  transform: ${({ visible }) =>
    visible ? `translateY(0)` : `translateY(${rem(-20)})`};
  opacity: ${({ visible }) => (visible ? 1 : 0)};
  transition: all 0.5s;
  width: 100%;
  max-width: ${({ theme }) => rem(theme.sizes.s)};
`;

const bodyOverflowHiddenClass = "modal-opened";

const Global = createGlobalStyle`
  body.${bodyOverflowHiddenClass} {
    overflow: hidden;
  }
`;

export default class Modal extends React.PureComponent<Props, State> {
  public modal: any;
  constructor(props: Props) {
    super(props);
    this.state = {
      largeModal: false
    };
  }

  public componentWillReceiveProps(nextProps: Props) {
    const { visible } = this.props;
    if (nextProps.visible && !visible) {
      document.body.classList.add(bodyOverflowHiddenClass);
    }
    if (!nextProps.visible && visible) {
      document.body.classList.remove(bodyOverflowHiddenClass);
    }
  }

  public componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside);
    // document.addEventListener("keypress", this.handleClickEsc, false);
    if (typeof window !== undefined && this.modal) {
      const windowHeight = window.innerHeight;
      const modalheight = this.modal.offsetHeight;
      if (modalheight > windowHeight) {
        this.setState({ largeModal: true });
      }
    }
  }

  public componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  // public handleClickEsc = (e: KeyboardEvent) => {
  //   console.log(e);
  //   if (e.keyCode === 27) {
  //     this.props.handleCloseModal();
  //   }
  // };

  public handleClickOutside = (e: any) => {
    if (this.modal && !this.modal.contains(e.target) && this.props.visible) {
      this.props.handleCloseModal();
    }
  };

  public setModal = (node: any) => {
    this.modal = node;
  };

  public render() {
    const { largeModal } = this.state;
    const { visible, children } = this.props;
    return (
      <Wrapper visible={visible} isLarge={largeModal}>
        <Global />
        <ModalContent visible={visible} ref={this.setModal}>
          {children}
        </ModalContent>
      </Wrapper>
    );
  }
}
