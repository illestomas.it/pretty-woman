import * as React from "react";
import { Page as PageType } from "src/services/types";
import { NavLink } from "react-router-dom";
import styled from "src/theme/styledComponents";
import { rem } from "polished";

interface Props {
  pages: PageType[];
}

const Wrapper = styled.div`
  padding: ${rem(15)};
  display: flex;
  flex-direction: column;
`;

const StyledNavLink = styled(NavLink)`
  font-size: ${rem(18)};
  margin-bottom: ${rem(10)};
  color: #fff;
`;

const Menu: React.SFC<Props> = ({ pages }) => {
  return (
    <Wrapper>
      {pages.length > 0 &&
        pages.map(page => (
          <StyledNavLink to={`/${page.slug}`}>
            {page.title.rendered}
          </StyledNavLink>
        ))}
    </Wrapper>
  );
};

export default Menu;
