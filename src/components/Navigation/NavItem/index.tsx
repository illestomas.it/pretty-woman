import * as React from "react";
import styled, { withTheme } from "src/theme/styledComponents";
import { rem } from "polished";
import { Page as PageType } from "src/services/types";
import { NavLink } from "react-router-dom";
import { ThemeInterface } from "src/theme/types";

const StyledLink = styled(NavLink)`
  font-size: ${rem(16)};
  padding: ${rem(5)} 0;
  color: ${({ theme }) => theme.colors.textPrimary};
  text-decoration: none;
  text-transform: uppercase;
  font-weight: 300;
  margin-right: ${rem(50)};
`;

interface Props {
  page: PageType;
  theme: ThemeInterface;
}

const NavItem: React.SFC<Props> = ({ page, theme }) => {
  return (
    <StyledLink
      to={!page.link.includes(page.slug) ? "/" : `/${page.slug}`}
      activeStyle={{
        borderBottom: `${rem(2)} solid ${theme.colors.primary}`,
        fontWeight: 700
      }}
      exact={true}
    >
      {page.title.rendered}
    </StyledLink>
  );
};

export default withTheme(NavItem);
