import * as React from "react";
import { rem } from "polished";
import styled from "src/theme/styledComponents";

interface Props {
  className?: string;
  color: string;
  height: number;
}

const Wrapper = styled("div")<{ color: string; height: number }>`
  position: absolute;
  bottom: 0;
  width: 100%;
  height: ${({ height }) => rem(height)};
  background: linear-gradient(rgba(255, 255, 255, 0), ${({ color }) => color});
`;

const Overlay: React.SFC<Props> = ({ color, className, height }) => {
  return <Wrapper height={height} color={color} className={className} />;
};

export default Overlay;
