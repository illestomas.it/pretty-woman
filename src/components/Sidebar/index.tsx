import * as React from "react";
import styled, { createGlobalStyle } from "src/theme/styledComponents";
import { rem } from "polished";

interface Props {
  isOpened: boolean;
  children: React.ReactChild;
}

const Wrapper = styled("div")<{ isOpened: boolean }>`
  height: 100vh;
  top: 0;
  position: fixed;
  z-index: 15;
  left: ${({ isOpened }) => (isOpened ? 0 : rem(-300))};
  width: ${rem(300)};
  background: ${({ theme }) => theme.colors.primary};
  transition: all 0.5s ease-in-out;
`;

const bodyOverflowHiddenClass = "sidebar-opened";

const Hidden = createGlobalStyle`
  body.${bodyOverflowHiddenClass} {
    overflow: hidden;
  }
`;

class Sidebar extends React.PureComponent<Props> {
  public componentWillReceiveProps(nextProps: Props) {
    if (nextProps.isOpened && !this.props.isOpened) {
      document.body.classList.add(bodyOverflowHiddenClass);
    }
    if (!nextProps.isOpened && this.props.isOpened) {
      document.body.classList.remove(bodyOverflowHiddenClass);
    }
  }

  public render() {
    const { isOpened, children } = this.props;
    return (
      <Wrapper isOpened={isOpened}>
        <Hidden />
        {children}
      </Wrapper>
    );
  }
}

export default Sidebar;
