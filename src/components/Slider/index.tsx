import * as React from "react";
import { Post } from "src/services/types";
import "react-image-gallery/styles/css/image-gallery.css";
import styled, { withTheme } from "src/theme/styledComponents";
import { rem } from "polished";
// import CarouselItem from "src/components/CarouselItem";
import { getProp } from "src/utilities";
import ImageGallery from "react-image-gallery";
import Icon from "../Icon";
import { ThemeInterface } from "src/theme/types";
import { Container } from "../Layout";
import Overlay from "../Overlay";

interface Props {
  posts: Post[];
  className?: string;
  theme: ThemeInterface;
}

interface Item {
  bulletClass: string;
  description: string;
  original: string;
  originalTitle: string;
  excerpt: string;
}

const Wrapper = styled("div")`
  & button.bullet {
    box-shadow: none !important;
  }
`;

const ItemWrapper = styled("div")`
  position: relative;
  display: flex;
  max-height: ${rem(500)};
`;

const ItemImg = styled.img`
  object-fit: cover;
`;

const ItemMeta = styled(Container)`
  position: absolute;
  display: flex;
  flex-direction: column;
  justify-content: center;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  color: #fff;
`;

const ItemTitle = styled("div")`
  font-size: ${rem(48)};
  font-weight: 300;
  line-height: 1.2;
  margin-bottom: ${rem(20)};
`;

const ItemDesc = styled("div")`
  font-size: ${rem(18)};
`;

const Arrow = styled("div")`
  background: none;
  border: none;
  position: absolute;
  z-index: 10;
  cursor: pointer;
`;

const LeftArrow = styled(Arrow)`
  top: 50%;
  left: ${rem(20)};
  transform: translateY(-50%);
`;

const RightArrow = styled(Arrow)`
  top: 50%;
  right: ${rem(20)};
  transform: translateY(-50%);
`;

class Slider extends React.PureComponent<Props> {
  public renderLeftNav = (onClick: any, disabled: boolean) => {
    const { theme } = this.props;
    return (
      <LeftArrow onClick={onClick}>
        <Icon
          name="left-arrow"
          color={theme.colors.sliderArrows}
          width="50"
          height="50"
        />
      </LeftArrow>
    );
  };

  public renderRightNav = (onClick: any, disabled: boolean) => {
    const { theme } = this.props;
    return (
      <RightArrow onClick={onClick}>
        <Icon
          name="right-arrow"
          color={theme.colors.sliderArrows}
          width="50"
          height="50"
        />
      </RightArrow>
    );
  };

  public renderItem = (item: Item) => {
    const { theme } = this.props;
    return (
      <ItemWrapper>
        <Overlay color={theme.colors.sliderOverlay} height={400} />
        <ItemMeta>
          <ItemTitle>{item.originalTitle}</ItemTitle>
          <ItemDesc dangerouslySetInnerHTML={{ __html: item.excerpt }} />
        </ItemMeta>
        <ItemImg src={item.original} />
      </ItemWrapper>
    );
  };

  public render() {
    const { posts, className } = this.props;
    const images: any = [];
    posts.map(post => {
      const image = getProp(post, "better_featured_image.source_url", "");
      const obj = {
        bulletClass: "bullet",
        description: post.title.rendered,
        excerpt: post.excerpt.rendered,
        original: image,
        originalTitle: post.title.rendered
      };
      images.push(obj);
    });
    return (
      <Wrapper className={className}>
        <ImageGallery
          items={images}
          showThumbnails={false}
          showFullscreenButton={false}
          showPlayButton={false}
          showBullets={true}
          renderLeftNav={this.renderLeftNav}
          renderRightNav={this.renderRightNav}
          renderItem={this.renderItem}
        />
      </Wrapper>
    );
  }
}

export default withTheme(Slider);
