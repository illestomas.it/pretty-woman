import * as React from "react";
import { Post as PostType, Tag as TagType } from "src/services/types";
import styled, { withTheme } from "src/theme/styledComponents";
import { rem } from "polished";
import * as moment from "moment";
import Icon from "src/components/Icon";
import { ThemeInterface } from "src/theme/types";

interface Props {
  post: PostType;
  tags: TagType[];
  theme: ThemeInterface;
}

const Title = styled.h1`
  color: ${({ theme }) => theme.colors.textPrimary};
  font-size: ${rem(48)};
`;

const Date = styled.div`
  color: ${({ theme }) => theme.colors.incativeTextColor};
  display: flex;
`;

const ClockIcon = styled(Icon)`
  margin-right: ${rem(10)};
`;

const PostHeader: React.SFC<Props> = ({ post, theme }) => {
  const date = moment(post.date).format("LLL");
  const title = post.title ? post.title.rendered : "";
  return (
    <React.Fragment>
      <Title>{title}</Title>
      <Date>
        <ClockIcon
          name="clock"
          width="20"
          height="20"
          color={theme.colors.incativeTextColor}
        />
        {date}
      </Date>
    </React.Fragment>
  );
};

export default withTheme(PostHeader);
