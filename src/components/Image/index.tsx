import * as React from "react";
import styled from "src/theme/styledComponents";

interface Props {
  className?: string;
  src: string;
}

interface State {
  loaded: boolean;
}

const Wrapper = styled("img")<{ loaded: boolean }>`
  max-width: 100%;
  opacity: ${({ loaded }) => (loaded ? "1" : "0")};
  transition: opacity 0.5s;
`;

class Image extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      loaded: false
    };
  }

  public render() {
    const { src, className } = this.props;
    const { loaded } = this.state;
    return (
      <Wrapper
        loaded={loaded}
        src={src}
        // tslint:disable
        onLoad={() => this.setState({ loaded: true })}
        className={className}
        // tslint:enable
      />
    );
  }
}

export default Image;
