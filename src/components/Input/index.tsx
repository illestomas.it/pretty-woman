import * as React from "react";
import styled from "src/theme/styledComponents";
import { rem } from "polished";

interface Props {
  value: string;
  handleChangeQuery: (event: React.ChangeEvent<HTMLInputElement>) => void;
  query: string;
}

const InputElement = styled("input")`
  border: 0;
  width: 100%;
  padding: ${rem(20)};
`;

const Input: React.StatelessComponent<Props> = ({
  query,
  handleChangeQuery
}) => {
  return <InputElement value={query} onChange={handleChangeQuery} />;
};

export default Input;
