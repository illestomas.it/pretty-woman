import * as React from "react";
import styled, { withTheme } from "src/theme/styledComponents";
import Icon from "src/components/Icon";
import { ThemeInterface } from "src/theme/types";

interface Props {
  handleOpenModal: () => void;
  theme: ThemeInterface;
  handleOpenSidebar: () => void;
}

const Wrapper = styled.div`
  display: flex;
  position: relative;
`;

const IconWrapper = styled("div")`
  display: flex;
  align-items: center;
  cursor: pointer;
  margin-left: auto;
`;

const StyledNavIcon = styled(Icon)`
  cursor: pointer;
`;

class Navigation extends React.PureComponent<Props> {
  public handleClickSearch = () => {
    this.props.handleOpenModal();
  };

  public render() {
    const { theme, handleOpenSidebar } = this.props;
    return (
      <Wrapper>
        <StyledNavIcon
          name="hamburger"
          color={theme.colors.textPrimary}
          width="20"
          height="20"
          onClick={handleOpenSidebar}
        />
        <IconWrapper onClick={this.handleClickSearch}>
          <Icon name="search" color="#777" width="20" height="20" />
        </IconWrapper>
      </Wrapper>
    );
  }
}

export default withTheme(Navigation);
