import * as React from "react";
import styled from "src/theme/styledComponents";
import LogoImg from "src/assets/img/logo.svg";
import { rem } from "polished";
import { Page as PageType, Post } from "src/services/types";
import Navigation from "src/containers/Navigation";
import { Container } from "src/components/Layout";
import { NavLink } from "react-router-dom";

const Wrapper = styled(Container)`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const LogoWrap = styled(NavLink)`
  display: flex;
  justify-content: center;
  margin: ${rem(60)} 0;
  align-self: center;

  & img {
    height: ${rem(60)};
  }
`;

interface Props {
  pages: PageType[];
  handleChangeQuery: (event: React.ChangeEvent<HTMLInputElement>) => void;
  query: string;
  searchResults: Post[];
  handleOpenModal: () => void;
  handleOpenSidebar: () => void;
  sidebarOpened: boolean;
}

interface State {
  sidebarOpened: boolean;
}

class Header extends React.PureComponent<Props, State> {
  public render() {
    const { handleOpenModal, handleOpenSidebar } = this.props;
    return (
      <Wrapper>
        <LogoWrap to="/">
          <img src={LogoImg} />
        </LogoWrap>
        <Navigation
          handleOpenModal={handleOpenModal}
          handleOpenSidebar={handleOpenSidebar}
        />
      </Wrapper>
    );
  }
}

export default Header;
