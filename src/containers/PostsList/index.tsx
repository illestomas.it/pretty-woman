import * as React from "react";
import { Post, Tag } from "src/services/types";
import PostTab from "src/components/PostTab";
import styled from "src/theme/styledComponents";
import { rem } from "polished/lib/index";

interface Props {
  title?: string;
  posts: Post[];
  tags: Tag[];
}

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const Title = styled.h2`
  color: ${({ theme }) => theme.colors.textPrimary};
  margin: 0;
  padding: ${rem(50)} 0;
  border-top: ${rem(1)} solid ${({ theme }) => theme.colors.separatorColor};
`;

const PostsList: React.SFC<Props> = ({ posts, tags, title }) => {
  return (
    <React.Fragment>
      {posts.length > 0 && (
        <React.Fragment>
          {title && <Title>{title}</Title>}
          <Wrapper>
            {posts.map(post => (
              <PostTab key={post.id} post={post} tags={tags} />
            ))}
          </Wrapper>
        </React.Fragment>
      )}
    </React.Fragment>
  );
};

export default PostsList;
