import * as React from "react";
import styled from "src/theme/styledComponents";
import { Tag as TagType } from "src/services/types";
import Tag from "src/components/Tag";
import { rem } from "polished";

interface Props {
  className?: string;
  tags: TagType[];
}

const Wrapper = styled.div`
  display: flex;
`;

const StyledTag = styled(Tag)`
  margin-right: ${rem(5)};
`;

const TagsList: React.StatelessComponent<Props> = ({ tags, className }) => {
  return (
    <Wrapper className={className}>
      {tags.length > 0 && tags.map(tag => <StyledTag key={tag.id} tag={tag} />)}
    </Wrapper>
  );
};

export default TagsList;
