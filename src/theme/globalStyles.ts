import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  body {
    margin: 0 !important;
    padding: 0;
    height: 100%;
    width: 100%;
    background: #f1f2f6;
  }
  html {
    font-size: 16px;
    font-family: 'Zilla slab', sans-serif;
  }
  * {
    box-sizing: border-box;
  }
  #root {
    height: 100%;
    width: 100%;
  }

  a {
    text-decoration: none;
    color: inherit;
  }
`;
