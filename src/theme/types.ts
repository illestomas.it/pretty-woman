export interface ThemeInterface {
  colors: {
    pageBackground: string;
    primary: string;
    textPrimary: string;
    sliderArrows: string;
    sliderOverlay: string;
    incativeTextColor: string;
    tagBackground: string;
    separatorColor: string;
  };
  media: {
    l: (...str: TemplateStringsArray[]) => any;
    m: (...str: TemplateStringsArray[]) => any;
    s: (...str: TemplateStringsArray[]) => any;
    sm: (...str: TemplateStringsArray[]) => any;
    xl: (...str: TemplateStringsArray[]) => any;
    xs: (...str: TemplateStringsArray[]) => any;
  };
  sizes: {
    l: number;
    m: number;
    s: number;
    sm: number;
    xl: number;
    xs: number;
  };
}
