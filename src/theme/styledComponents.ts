// styled-components.ts
import * as styledComponents from "styled-components";
import { ThemedStyledComponentsModule } from "styled-components";
import { GlobalStyle } from "./globalStyles";
import { ThemeInterface } from "./types";

const {
  default: styled,
  css,
  createGlobalStyle,
  keyframes,
  ThemeProvider,
  withTheme
} = styledComponents as ThemedStyledComponentsModule<ThemeInterface>;

interface Theme {
  theme: ThemeInterface;
}

export {
  css,
  createGlobalStyle,
  GlobalStyle,
  keyframes,
  ThemeProvider,
  withTheme,
  Theme
};

export default styled;
