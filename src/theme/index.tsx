import { css } from "./styledComponents";
import { rem, rgba } from "polished";

const primary = "#ED9DC4";

const sizes = {
  l: 1100,
  m: 992,
  s: 600,
  sm: 900,
  xl: 1200,
  xs: 450
};

const media = {
  l: (...args: TemplateStringsArray[]) => css`
    @media (min-width: ${rem(sizes.l)}) {
      ${css.call(undefined, ...args)};
    }
  `,
  m: (...args: TemplateStringsArray[]) => css`
    @media (min-width: ${rem(sizes.m)}) {
      ${css.call(undefined, ...args)};
    }
  `,
  s: (...args: TemplateStringsArray[]) => css`
    @media (min-width: ${rem(sizes.s)}) {
      ${css.call(undefined, ...args)};
    }
  `,
  sm: (...args: TemplateStringsArray[]) => css`
    @media (min-width: ${rem(sizes.sm)}) {
      ${css.call(undefined, ...args)};
    }
  `,

  xl: (...args: TemplateStringsArray[]) => css`
    @media (min-width: ${rem(sizes.xl)}) {
      ${css.call(undefined, ...args)};
    }
  `,
  xs: (...args: TemplateStringsArray[]) => css`
    @media (min-width: ${rem(sizes.xs)}) {
      ${css.call(undefined, ...args)};
    }
  `
};

const theme = {
  colors: {
    incativeTextColor: "#aaa",
    pageBackground: "#f3f3f3",
    primary,
    separatorColor: "#dedede",
    sliderArrows: "#fff",
    sliderOverlay: primary,
    tagBackground: rgba(primary, 0.2),
    textPrimary: "#333"
  },
  media,
  sizes
};

export default theme;
