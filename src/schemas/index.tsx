import { schema } from "normalizr";

export const PostsSchema = new schema.Entity("posts", [], {
  idAttribute: "id"
});

export const PagesSchema = new schema.Entity("pages", [], {
  idAttribute: "id"
});

export const TagsSchema = new schema.Entity("tags", [], {
  idAttribute: "id"
});
